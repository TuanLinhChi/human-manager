import React, { Component } from 'react';
import Control from './components/Control'
import Content from './components/Content'
import './App.css';

//import all
// import _ from 'lodash';
import { findIndex } from 'lodash';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            //id, name, description, state
            tasks : [],
            idMax : 0,
            stateAdd : false,
            edittingTask : null,
            filter : {
                name : '',
                state : -1
            },
            keyWord : '',
            sort : 0
        }
    }

    // refresh chỉ dc gọi 1 lần
    componentWillMount(){
        if(localStorage && localStorage.getItem('tasks') && localStorage.getItem('idMax')){
            //chuyển về dạng object
            var tasks = JSON.parse(localStorage.getItem('tasks'));
            var idMax = parseInt(localStorage.getItem('idMax'));
            //lưu vào state
            this.setState({
                tasks : tasks,
                idMax : idMax
            });
        }
    }

    changeStateAdd=()=>{
        this.setState({
            stateAdd : true,
            edittingTask : null,
        });
    }

    removeAddTask=()=>{
        this.setState({
            stateAdd : false,
            edittingTask : null,
        });
    }

    onSubmit=(data)=>{
        var {tasks, idMax} = this.state;
        if(data.id === null){
            //submit add
            idMax++;
            data.id = idMax;
            tasks.push(data);
            localStorage.setItem('idMax',idMax);
        }else{
            //submit edit
            let key = this.findIndex(data.id);
            tasks[key].name = data.name;
            tasks[key].description = data.description;
            tasks[key].state = data.state;
            this.removeAddTask();
        }
        localStorage.setItem('tasks', JSON.stringify(tasks));
        this.setState({
            tasks : tasks,
            idMax : idMax,
            edittingTask : null,    
        });
    }

    findIndex=(id)=>{
        let {tasks} = this.state;
        let result = -1;
        tasks.forEach((task, key)=>{
            if(task.id === id) result = key;
        });
        return result;
    }

    onChangeState=(id, state)=>{
        let {tasks} = this.state;
        // let key = this.findIndex(id);
        //findIndex of lodash
        let key = findIndex(tasks, (task)=>{
            return task.id === id;
        });
        if(key !== -1){
            tasks[key].state = state;
        }
        this.setState({
            tasks: tasks
        });
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    removeTask=(id)=>{
        let {tasks} = this.state;
        let key = this.findIndex(id);
        if(key !== -1){
            tasks.splice(key, 1);
        }
        this.setState({
            tasks: tasks
        });
        localStorage.setItem('tasks', JSON.stringify(tasks));
        this.removeAddTask();
    }

    onShowEditting=(id)=>{
        let {tasks} = this.state;
        let key = this.findIndex(id);
        if(key !== -1){
            var edittingTask = tasks[key];
        }
        this.changeStateAdd();
        this.setState({
            edittingTask : edittingTask
        });
    }

    onEditTask=(no, name, description)=>{
        let {tasks} = this.state;
        let key = this.findIndex(no);
        if(key !== -1){
            tasks[key].name = name;
            tasks[key].description = description;
        }
        this.setState({
            tasks: tasks
        });
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    onFilter=(filterName, filterStatus)=>{
        this.setState({
            filter : {
                name : filterName.toLowerCase(),
                state : parseInt(filterStatus)
            }
        })
    }

    onFilterKey=(keyWord)=>{
        this.setState({
            keyWord : keyWord
        });
    }

    onSort=(sort)=>{
        this.setState({
            sort : parseInt(sort)
        });
    }

    render() {
        //same same, var tasks = this.state.tasks
        var {tasks, filter, keyWord, sort} = this.state;
        if(filter){
            if(filter.name){
                tasks = tasks.filter((task)=>{
                    return task.name.toLowerCase().indexOf(filter.name) !== -1
                });
            }

            tasks = tasks.filter((task)=>{
                if(filter.state === -1){
                    return task;
                }else{
                    return parseInt(task.state) === filter.state
                }
            });
        }
        
        if(keyWord){
            tasks = tasks.filter((task)=>{
                return task.name.toLowerCase().indexOf(keyWord) !== -1
            });
        }

        tasks = tasks.sort((task1, task2)=>{
            if(task1.name > task2.name){
                return sort;
            }else if(task1.name < task2.name){
                return -sort;
            }else{
                return 0
            }
        });
        
        return (
            <div className="container">
                <div className="row mt-15 text-center mb-5 mt-5 pl-5">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-3 text-danger">
                        <h1>My Task Manager</h1>
                    </div>
                    <Control
                        onFilterKey={ this.onFilterKey }
                        changeStateAdd={this.changeStateAdd}
                    />
                </div>
                <Content 
                    tasks={tasks} 
                    stateAdd={this.state.stateAdd} 
                    removeAddTask={this.removeAddTask}
                    onSubmit={this.onSubmit}
                    onChangeState={this.onChangeState}
                    removeTask={this.removeTask}
                    onShowEditting={this.onShowEditting}
                    edittingTask={this.state.edittingTask}
                    onEditTask={this.onEditTask}
                    onFilter={ this.onFilter }
                    onSort={ this.onSort }
                />
            </div>
        );
    }
}

export default App;
