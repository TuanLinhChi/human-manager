import React, { Component } from 'react';
import ItemTask from './ItemTask'
import FilterTask from './FilterTask'

class ListTasks extends Component {
    render() {
        var {tasks, stateAdd} = this.props;
        var elements = tasks.map((task)=>{
            return <ItemTask 
                        key={ task.id } 
                        task={ task }
                        no={ task.id }
                        onChangeState={this.props.onChangeState}
                        removeTask={this.props.removeTask}
                        onShowEditting={this.props.onShowEditting}
                        onEditTask={this.props.onEditTask}
                    />
        });
        return (
            <div className={stateAdd===true?"col-xs-8 col-sm-8 col-md-8 col-lg-8":"col-xs-12 col-sm-12 col-md-12 col-lg-12"}>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th className="text-center">#</th>
                            <th className="text-center">Name</th>
                            <th className="text-center">State</th>
                            <th className="text-center">Description</th>
                            <th className="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <FilterTask 
                            onFilter={ this.props.onFilter }
                            onSort={ this.props.onSort }
                        />
                        { elements }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default ListTasks;
