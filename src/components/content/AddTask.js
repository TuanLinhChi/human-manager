import React, { Component } from 'react';

class AddTask extends Component {
    constructor(props){
        super(props);
        this.state = {
            name : '',
            description : '',
            state : 1,
            id : null,
        }
    }

    componentWillMount(){
        var {edittingTask} = this.props;
        if(edittingTask !== null){
            this.setState({
                id : edittingTask.id,
                name : edittingTask.name,
                description : edittingTask.description,
                state : edittingTask.state,
            });
        }
    }

    //mỗi lần có sự thay đổi thì hàm này sẽ sử dụng lại
    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.edittingTask !== null){
            this.setState({
                id : nextProps.edittingTask.id,
                name : nextProps.edittingTask.name,
                description : nextProps.edittingTask.description,
                state : nextProps.edittingTask.state,
            });
        }else if(nextProps && nextProps.edittingTask === null){
            this.setState({
                name : '',
                description : '',
                state : 1,
                id : null,
            });
        }
    }

    onChange=(event)=>{
        var name = event.target.name;
        var value = event.target.value;
        value = name === 'state'?parseInt(value):value;
        this.setState({
            [name] : value
        });
    }

    onClear=()=>{
        this.setState({
            name : '',
            description : '',
            state : 1,
            id : null,
        });
    }

    onSubmit=(event)=>{
        event.preventDefault();
        this.props.onSubmit(this.state);
        this.onClear();
    }

  render() {
    var {id, name, description, state} = this.state;
    return (
        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div className="panel panel-warning border border-info rounded">
                <div className="panel-heading">
                    <h3 className="panel-title bg-warning pr-4 pl-4 pt-2 pt-2">
                        <label className="col-10 p-0">{id!==null?"Edit":"Add"} Task</label>
                        <button 
                            className="btn btn-outline-light border-0"
                            onClick={this.props.removeAddTask}
                        >
                            <span className="fa fa-times-circle text-danger"></span>
                        </button>
                    </h3>
                </div>
                <div className="panel-body pb-4 pl-4 pr-4 pt-0">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label>Name :</label>
                            <input
                                type="text"
                                className="form-control"
                                name="name"
                                value={id!==null?name:this.state.name}
                                onChange={this.onChange}
                            />
                        </div>

                        <div className="form-group">
                            <label>Description :</label>
                            <input
                                type="text"
                                className="form-control"
                                name="description"
                                value={id!==null?description:this.state.description}
                                onChange={this.onChange}
                            />
                        </div>

                        <label>State :</label>
                        <select
                            className="form-control"
                            name="state"
                            value={id!==null?state:this.state.state}
                            onChange={this.onChange}
                        >
                            <option value={1}>List</option>
                            <option value={2}>Working</option>
                            <option value={3}>Review</option>
                            <option value={4}>Completed</option>
                        </select><br/>
                        <div className="text-center">
                            <button 
                                type="reset" 
                                className="btn btn-primary"
                                onClick={this.onClear}
                            >
                                <span className="fa fa-undo mr-2"></span>Reset
                            </button>&nbsp;
                            <button type="submit" className="btn btn-success ml-4">
                                <span className="fa fa-save mr-2"></span>Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
  }
}

export default AddTask;
