import React, { Component } from 'react';
import AddTask from './content/AddTask'
import ListTasks from './content/ListTasks'

class Content extends Component {
  render() {
    var {tasks, stateAdd} = this.props;
    var elementAdd = stateAdd===true?
        <AddTask 
            removeAddTask={this.props.removeAddTask} 
            onSubmit={this.props.onSubmit} 
            edittingTask={this.props.edittingTask}
        />:null
    return (
        <div className="row mt-15">
            <ListTasks 
                tasks={tasks} 
                stateAdd={stateAdd}
                onChangeState={this.props.onChangeState}
                removeTask={this.props.removeTask}
                onShowEditting={this.props.onShowEditting}
                onEditTask={this.props.onEditTask}
                onFilter={ this.props.onFilter }
                onSort={ this.props.onSort }
            />
            {elementAdd}
        </div>
    );
  }
}

export default Content;
