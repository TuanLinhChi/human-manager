import React, { Component } from 'react';

class Add extends Component {
    render() {
        return (
            <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <button 
                    className="btn btn-success mr-2"
                    onClick={this.props.changeStateAdd}
                >
                    <span className="fa fa-plus mr-2"></span>Add Task
                </button>
            </div>
        );
    }
}

export default Add;
